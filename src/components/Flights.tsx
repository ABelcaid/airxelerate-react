import React, { useCallback, useState } from "react";
import "devextreme/dist/css/dx.common.css";
import "devextreme/dist/css/dx.light.css";
import DataGrid, { Selection } from "devextreme-react/data-grid";
import flights from "../data/flights.json";
import FlightDetail from "./FlightDetail";

const Flights: React.FC = () => {
  const [flightNumber, setFlightNumber] = useState<string>("");

  const getFlightNumber = useCallback((e) => {
    const flightNumber = e.selectedRowsData[0].flight_number;

    setFlightNumber(flightNumber);
  }, []);

  return (
    <div className="App">
      <h4>Flight lists</h4>
      <DataGrid dataSource={flights} onSelectionChanged={getFlightNumber}>
        <Selection mode="single" />
      </DataGrid>
      <FlightDetail flightNumber={flightNumber} />
    </div>
  );
};

export default Flights;
