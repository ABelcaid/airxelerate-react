import React from "react";
import Flights from "./components/Flights";

const App: React.FC = () => {
  return (
    <div className="App">
      <nav className="menu" tabIndex={0}>
        <div className="smartphone-menu-trigger" />
        <header className="avatar">
         
          <h6>Airxelerate </h6>
        </header>
        <ul>
          <li tabIndex={0} className="icon-aircraft">
            <span>Flight</span>
          </li>
          <li tabIndex={0} className="icon-aircraftDetails">
            <span>Flight Detail</span>
          </li>
          <li tabIndex={0} className="icon-settings">
            <span>Log out</span>
          </li>
        </ul>
      </nav>
      <main>
        <div className="helper">
          All Flight<span> Management | Flight</span>
        </div>

        <Flights />
      </main>
    </div>
  );
};

export default App;
