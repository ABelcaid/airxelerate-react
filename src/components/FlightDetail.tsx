import React from "react";
import "devextreme/dist/css/dx.common.css";
import "devextreme/dist/css/dx.light.css";
import DataGrid from "devextreme-react/data-grid";
import contingents from "../data/contingents.json";

interface Props {
  flightNumber: string;
}

const FlightDetail: React.FC<Props> = ({ flightNumber }) => {
  if (flightNumber) {
    const filtredFlight = contingents.filter(
      (item) => item.flight_number === flightNumber
    );
    const data = filtredFlight[0].contingents;

    return (
      <div>
        <h4>Flight Details</h4>
        <DataGrid dataSource={data}></DataGrid>
      </div>
    );
  }
  return null;
};

export default FlightDetail;
